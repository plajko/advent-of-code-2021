package hu.plajko.adventofcode2021;

import hu.plajko.adventofcode2021.utils.Extensions;
import lombok.SneakyThrows;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day20 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day20.class, args);
    }

    @Override
    @SneakyThrows
    public void run(String... args) {
        var input = "day20.in".readLines();
        var algorithmString = input.remove(0);
        var algorithm = IntStream.range(0, 512).boxed().collect(Collectors.toMap(
                i -> Integer.toBinaryString(i).pad(9, '0'),
                i -> algorithmString.charAt(i) == '#' ? 1 : 0));
        input.remove(0);
        var image = input.stream()
                .map(l -> l.chars().mapToObj(c -> c == '#' ? 1 : 0).toList())
            .toList();

        Function<List<List<Integer>>, Integer> sum = l ->
            l.stream().flatMap(List::stream).mapToInt(Integer::intValue).sum();

        for (int i = 0; i < 50; i++) {
            image = apply(algorithm, image, algorithmString.startsWith("#") ? i % 2 : 0);
            if (i == 1) {
                LOG.info("solution1: {}", sum.apply(image));
            }
        }
        LOG.info("solution2: {}", sum.apply(image));
    }

    List<List<Integer>> apply(Map<String, Integer> algorithm, List<List<Integer>> image, int blank) {
        return IntStream.range(0, image.size() + 2).mapToObj(x ->
            IntStream.range(0, image.get(0).size() + 2).mapToObj(y ->
                IntStream.range(0, 9).mapToObj(i -> {
                    int ox = x - 2 + i / 3;
                    int oy = y - 2 + i % 3;
                    if (ox < 0 || ox >= image.size() || oy < 0 || oy >= image.get(0).size()) {
                        return blank;
                    }
                    return image.get(ox).get(oy);
                })
                .map(String::valueOf).collect(Collectors.joining()))
            .map(algorithm::get).toList())
        .toList();
    }
}
