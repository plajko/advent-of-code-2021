package hu.plajko.adventofcode2021;

import hu.plajko.adventofcode2021.utils.Extensions;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day05 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day05.class, args);
    }

    @Value
    static class Point {
        int x; int y;
        Stream<Point> lineTo(Point to) {
            int xdir = Integer.signum(to.getX() - this.getX());
            int ydir = Integer.signum(to.getY() - this.getY());
            return Stream.iterate(this,
                p -> (Integer.compare(to.getX(), p.getX()) != -xdir
                   || Integer.compare(to.getY(), p.getY()) != -ydir),
                p -> new Point(p.getX() + xdir, p.getY() + ydir));
        }
        boolean horizontalOrVertical(Point to) {
            return this.getX() == to.getX() || this.getY() == to.getY();
        }
    }

    @Override
    @SneakyThrows
    public void run(String... args) {
        var lines = "day05.in".readLines().stream()
                .map("(\\d+),(\\d+) -> (\\d+),(\\d+)".asMatcher())
            .map(m -> new Point[] {
                    new Point(m.g(1).asInt(), m.g(2).asInt()),
                    new Point(m.g(3).asInt(), m.g(4).asInt())
            }).toList();

        Function<Stream<Point[]>, Long> countOverlap = s -> s
            .flatMap(l -> l[0].lineTo(l[1]))
            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
            .entrySet().stream().filter(e -> e.getValue() > 1).count();

        var s1 = countOverlap.apply(lines.stream().filter(l -> l[0].horizontalOrVertical(l[1])));
        LOG.info("solution1: {}", s1);
        var s2 = countOverlap.apply(lines.stream());
        LOG.info("solution2: {}", s2);
    }
}
