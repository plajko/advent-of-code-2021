package hu.plajko.adventofcode2021;

import hu.plajko.adventofcode2021.utils.Extensions;
import lombok.SneakyThrows;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.*;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day09 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day09.class, args);
    }

    @Override
    @SneakyThrows
    public void run(String... args) {
        var input = "day09.in".readLines().stream()
            .map(l -> "\\d".allMatches(l, Integer::parseInt).toList()).toList();

        int lowPointsSum = 0;
        List<Integer> basins = new ArrayList<>();
        for (int i = 0; i < input.size(); i++) {
            for (int j = 0; j < input.get(0).size(); j++) {
                int num = input.get(i).get(j);
                int e1 = i > 0 ? input.get(i - 1).get(j) : Integer.MAX_VALUE;
                int e2 = i < input.size() - 1 ? input.get(i + 1).get(j) :  Integer.MAX_VALUE;
                int e3 = j > 0 ? input.get(i).get(j - 1) :  Integer.MAX_VALUE;
                int e4 = j < input.get(0).size() - 1 ? input.get(i).get(j + 1) : Integer.MAX_VALUE;
                if (num < e1 && num < e2 && num < e3 && num < e4) {
                    lowPointsSum += num + 1;
                    var basin = new HashSet<List<Integer>>();
                    collect(i, j, -1, basin, input);
                    basins.add(basin.size());
                }
            }
        }
        basins.sort(Comparator.reverseOrder());
        LOG.info("solution1: {}", lowPointsSum);
        LOG.info("solution2: {}", basins.get(0) * basins.get(1) * basins.get(2));
    }

    void collect(int i, int j, int prev, Set<List<Integer>> basin, List<List<Integer>> input) {
        int current = input.get(i).get(j);
        if (current < 9 && current > prev) {
            if (basin.add(List.of(i, j))) {
                if (i > 0) {
                    collect(i - 1, j, current, basin, input);
                }
                if (i < input.size() - 1) {
                    collect(i + 1, j, current, basin, input);
                }
                if (j > 0) {
                    collect(i, j - 1, current, basin, input);
                }
                if (j < input.get(0).size() - 1) {
                    collect(i, j + 1, current, basin, input);
                }
            }
        }
    }

}
