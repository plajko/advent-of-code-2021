package hu.plajko.adventofcode2021.utils;

import com.google.common.base.Suppliers;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.io.Resources;
import lombok.SneakyThrows;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Extensions {

    @SneakyThrows
    public static List<String> readLines(String resourceFileName) {
        return Files.readAllLines(Path.of(Resources.getResource(resourceFileName).toURI()), StandardCharsets.UTF_8);
    }
    public static <T> List<List<T>> split(List<T> list, Predicate<T> condition) {
        final var groupIdState = new AtomicInteger();
        return new ArrayList<>(list.stream()
                .map(i -> Optional.of(condition.test(i))
                        .map(test -> Maps.immutableEntry(
                                groupIdState.getAndAdd(test ? 1 : 0),
                                Optional.ofNullable(test ? null : i)
                        )).get())
                .filter(e -> e.getValue().isPresent())
                .collect(Collectors.groupingBy(
                        Map.Entry::getKey,
                        TreeMap::new,
                        Collectors.mapping(
                                e -> e.getValue().get(),
                                Collectors.toList())
                ))
                .values());
    }

    public static int asInt(String s) {
        return Integer.parseInt(s);
    }
    public static int asInt(String s, int radix) {
        return Integer.parseInt(s, radix);
    }
    public static long asLong(String s) {
        return Long.parseLong(s);
    }
    public static long asLong(String s, int radix) {
        return Long.parseLong(s, radix);
    }
    public static String pad(String s, int size) {
        return pad(s, size, ' ');
    }
    public static String pad(String s, int size, char c) {
        return IntStream.range(0, Math.max(0, size - s.length())).mapToObj(i -> c + "").collect(Collectors.joining()) + s;
    }
    public static BigInteger asBigInteger(String s) {
        return new BigInteger(s);
    }
    public static String replaceAll(String s, String regex, Function<MatchResult, ?> replacement) {
        return Pattern.compile(regex).matcher(s).replaceAll(replacement.andThen(String::valueOf));
    }
    public static Function<CharSequence, MatchResult> asMatcher(String regex) {
        return s -> allMatches(regex, s).findFirst().orElse(null);
    }
    public static Stream<MatchResult> allMatches(String regex, CharSequence s) {
        return Stream.generate(Suppliers.memoize(() -> Pattern.compile(regex).matcher(s)))
            .takeWhile(Matcher::find).map(Matcher::toMatchResult);
    }
    public static <T> Stream<T> allMatches(String regex, CharSequence s, Function<String, T> mapper) {
        return allMatches(regex, s).map(mapper.compose(MatchResult::group));
    }
    public static String g(MatchResult result, int group) {
        return result.group(group);
    }
    public static String g(MatchResult result) {
        return result.group();
    }
    public static <T> T first(List<T> list) {
        return list.isEmpty() ? null : list.get(0);
    }
    public static <T> T last(List<T> list) {
        return list.isEmpty() ? null : list.get(list.size() - 1);
    }
    public static <T> List<T> toList(Stream<T> stream) {
        return stream.collect(Collectors.toList());
    }
    public static <T> Stream<T> flatStream(List<List<T>> matrix) {
        return matrix.stream().flatMap(List::stream);
    }
    public static <T> List<List<T>> transpose(List<List<T>> matrix) {
        return IntStream.range(0, matrix.get(0).size())
            .mapToObj(i -> matrix.stream()
                .map(l -> l.get(i))
                .collect(Collectors.toList()))
            .collect(Collectors.toList());
    }
    public static <T> List<List<T>> rotateCW(List<List<T>> matrix) {
        var reverse = Lists.reverse(matrix);
        return IntStream.range(0, reverse.get(0).size())
            .mapToObj(i -> reverse.stream()
                .map(l -> l.get(i))
                .collect(Collectors.toList()))
            .collect(Collectors.toList());
    }
    public static <T> List<List<T>> rotateCCW(List<List<T>> matrix) {
        return IntStream.range(0, matrix.get(0).size())
            .mapToObj(i -> matrix.stream()
                .map(l -> l.get(l.size() - 1 - i))
                .collect(Collectors.toList()))
            .collect(Collectors.toList());
    }
    public static <T> void dump(List<List<T>> matrix) {
        var widths = IntStream.range(0, matrix.get(0).size())
            .mapToObj(i -> matrix.stream().mapToInt(r -> String.valueOf(r.get(i)).length()).max().getAsInt())
            .collect(Collectors.toList());
        System.out.println(matrix.stream()
            .map(r -> IntStream.range(0, r.size())
                .mapToObj(i -> pad(String.valueOf(r.get(i)), widths.get(i)))
                .collect(Collectors.joining(" ")))
            .collect(Collectors.joining("\n")) + "\n");
    }

    public static <T> void dump(BiFunction<Integer, Integer, Object> provider, int x, int y) {
        var empty = " ";
        var widths = IntStream.range(0, y)
            .mapToObj(i -> IntStream.range(0, x)
                .map(j -> Optional.ofNullable(provider.apply(i, j))
                    .map(String::valueOf)
                    .orElse(empty).length())
                .max().getAsInt())
            .collect(Collectors.toList());
        System.out.println(IntStream.range(0, x)
            .mapToObj(i -> IntStream.range(0, y)
                .mapToObj(j -> Optional.ofNullable(provider.apply(i, j))
                    .map(String::valueOf)
                    .orElse(empty))
                    .map(s -> pad(s, widths.get(i)))
                    .collect(Collectors.joining(" ")))
                .collect(Collectors.joining("\n")) + "\n");
    }
}
