package hu.plajko.adventofcode2021.utils;

import lombok.experimental.ExtensionMethod;
import lombok.experimental.UtilityClass;

import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

@UtilityClass
@ExtensionMethod({Extensions.class})
public class AdventUtils {
    public static void main(String[] args) {
        "(\\d+)".allMatches("a 342 a a 4345 a").forEach(parsed -> {
            System.out.println(parsed.g(1));
        });
        Stream.of(
                "a 1",
                "a 2",
                "b 3",
                "asdasd 4453453234")
            .map("^(.*) (\\d+)".asMatcher())
            .filter(Objects::nonNull)
            .forEach(parsed -> {
                    System.out.println(parsed.g(1) + " - " + parsed.g(2));
                    System.out.println(parsed.g(1) + " - " + parsed.g(2).asLong());
                    System.out.println(parsed.g(1) + " - " + parsed.g(2).asBigInteger());
                }
            );
        System.out.println( "a 342 a a 4345 a 0 0 0 0 sdf adf3as".replaceAll("(\\d)\\d*", m -> m.g(1).asInt() + 1));

        var tt = List.of("a", "b", "", "c", "d").split(String::isEmpty);
        System.out.println(tt);
        System.out.println("0".pad(2));
    }

}
