package hu.plajko.adventofcode2021;

import com.google.common.collect.Lists;
import hu.plajko.adventofcode2021.utils.Extensions;
import lombok.SneakyThrows;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day13 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day13.class, args);
    }

    @Override
    @SneakyThrows
    public void run(String... args) {
        var input = "day13.in".readLines().split(String::isEmpty);
        var coords = input.get(0).stream()
            .map("(\\d+),(\\d+)".asMatcher()).map(m -> List.of(m.group(1).asInt(), m.group(2).asInt()))
            .collect(Collectors.toSet());

        var A = "#";
        var B = "";

        var map = new ArrayList<List<String>>();
        int xMax = coords.stream().mapToInt(c -> c.get(0)).max().getAsInt() + 1;
        int yMax = coords.stream().mapToInt(c -> c.get(1)).max().getAsInt() + 1;
        for (int i = 0; i < yMax; i++) {
            var row = new ArrayList<String>();
            for (int j = 0; j < xMax; j++) {
                row.add(coords.contains(List.of(j, i)) ? A : B);
            }
            map.add(row);
        }

        var folds = input.get(1).stream().map("fold along (.*)=(\\d+)".asMatcher()).map(m -> {
            int val = m.group(2).asInt();
            return m.group(1).equals("x") ? List.of(val, 0) : List.of(0, val);
        }).toList();

        BiFunction<String, String, String> merge = (v1, v2) -> v1.equals(A) ? v1 : v2;

        var s1 = fold(map, folds.get(0), merge).stream()
            .mapToLong(l -> l.stream().filter(A::equals).count()).sum();
        LOG.info("solution1: {}", s1);

        List<List<String>> folded = map;
        for (var fold : folds) {
            folded = fold(folded, fold, merge);
        }
        folded.dump();
        LOG.info("solution2: {}", "RKHFZGUB");
    }

    private List<List<String>> fold(List<List<String>> map, List<Integer> line, BiFunction<String, String, String> mergeFunction) {
        if (line.get(0) == 0) {
            // horizontal fold
            return merge(
                map.subList(0, line.get(1)),
                Lists.reverse(map.subList(line.get(1) + 1, map.size())),
                mergeFunction);
        } else {
            // vertical fold
            return fold(map.rotateCW(), List.of(0, line.get(0)), mergeFunction).rotateCCW();
        }
    }

    private List<List<String>> merge(
            List<List<String>> map1,
            List<List<String>> map2,
            BiFunction<String, String, String> mergeFunction) {
        val bigger = map1.size() < map2.size() ? map2 : map1;
        val smaller = map1.size() < map2.size() ? map1 : map2;
        return Stream.concat(
            bigger.subList(0, bigger.size() - smaller.size()).stream().map(ArrayList::new),
            IntStream.range(0, smaller.size()).mapToObj(i -> IntStream.range(0, bigger.get(i).size()).mapToObj(j ->
                mergeFunction.apply(bigger.get(bigger.size() - smaller.size() + i).get(j), smaller.get(i).get(j))
            ).toList())
        ).toList();
    }


}
