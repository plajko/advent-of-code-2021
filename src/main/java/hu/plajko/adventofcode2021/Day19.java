package hu.plajko.adventofcode2021;

import com.google.common.base.Suppliers;
import hu.plajko.adventofcode2021.utils.Extensions;
import lombok.EqualsAndHashCode;
import lombok.SneakyThrows;
import lombok.ToString;
import lombok.Value;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day19 implements CommandLineRunner {

    static int[][][] A = transform(new int[][][]{
            {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}},
            {{0, 1, 0}, {0, 0, 1}, {1, 0, 0}},
            {{0, 0, 1}, {1, 0, 0}, {0, 1, 0}}
    });
    static int[][][] B = transform(new int[][][]{
            {{1, 0, 0}, {-1, 0, 0}, {-1, 0, 0}, {1, 0, 0}},
            {{0, 1, 0}, {0, -1, 0}, {0, 1, 0}, {0, -1, 0}},
            {{0, 0, 1}, {0, 0, 1}, {0, 0, -1}, {0, 0, -1}}
    });
    static int[][][] C = transform(new int[][][]{
            {{1, 0, 0}, {0, 0, -1}},
            {{0, 1, 0}, {0, -1, 0}},
            {{0, 0, 1}, {-1, 0, 0}}
    });

    static int[][][] transform(int[][][] a) {
        return IntStream.range(0, a[0].length).mapToObj(i ->
            Arrays.stream(a).map(m -> m[i]).toArray(int[][]::new))
        .toArray(int[][][]::new);
    }

    public static void main(String[] args) {
        SpringApplication.run(Day19.class, args);
    }

    @Value
    static class Point {
        int x, y, z;

        Point transform(int[][] matrix) {
            return new Point(
                matrix[0][0] * x + matrix[0][1] * y + matrix[0][2] * z,
                matrix[1][0] * x + matrix[1][1] * y + matrix[1][2] * z,
                matrix[2][0] * x + matrix[2][1] * y + matrix[2][2] * z);
        }

        @EqualsAndHashCode.Exclude
        @ToString.Exclude
        Supplier<List<Point>> orientations = Suppliers.memoize(() ->
            Arrays.stream(A)
                .flatMap(a -> Arrays.stream(B).parallel()
                    .flatMap(b -> Arrays.stream(C).parallel()
                        .map(c -> this.transform(a).transform(b).transform(c)))).toList()
        );

        Point add(Point p) {
            return new Point(p.x + x, p.y + y, p.z + z);
        }

        Point negate() {
            return new Point(-x, -y, -z);
        }

        int manhattan() {
            return Math.abs(x) + Math.abs(y) + Math.abs(z);
        }
    }

    List<Set<Point>> allOrientations(Set<Point> points) {
        return points.stream().parallel().map(p -> p.getOrientations().get()).toList().transpose().stream()
                .<Set<Point>>map(HashSet::new).toList();
    }

    @Value
    static class Result {
        Set<Point> beacons;
        Point scannerLocation;
    }

    Result match(Set<Point> from, Set<Point> to, int minMatchCount) {
        return allOrientations(to).stream().parallel().flatMap(orientation ->
            from.stream().parallel().flatMap(candidate1 ->
                orientation.stream().limit(orientation.size() - minMatchCount).parallel().map(candidate2 -> {
                    var location = candidate1.add(candidate2.negate());
                    var count = orientation.stream()
                        .map(location::add)
                        .filter(from::contains)
                        .limit(minMatchCount).count();
                    if (count == minMatchCount) {
                        return Optional.of(new Result(
                            orientation.stream().map(location::add).collect(Collectors.toSet()),
                            location));
                    }
                    return Optional.<Result>empty();
                })
                .filter(Optional::isPresent).map(Optional::get)))
        .findFirst().orElse(null);
    }

    @Override
    @SneakyThrows
    public void run(String... args) {
        var input = "day19.in".readLines().split(String::isEmpty).stream()
            .collect(Collectors.groupingBy(
                s -> "--- (scanner \\d+) ---".asMatcher().apply(s.get(0)).g(1),
                Collectors.flatMapping(
                    s -> s.subList(1, s.size()).stream().map(l -> l.split((",")))
                        .map(m -> new Point(m[0].asInt(), m[1].asInt(), m[2].asInt())),
                    Collectors.toSet())));

        var beacons = new ArrayList<List<Set<Point>>>();
        beacons.add(List.of(input.remove("scanner 0")));
        var scanners = new ArrayList<Point>();
        scanners.add(new Point(0, 0, 0));

        while (!input.isEmpty()) {
            var moreBeacons = new ArrayList<Set<Point>>();
            var processed = new HashSet<String>();
            for (var t : beacons.last()) {
                for (var e : input.entrySet()) {
                    var m = match(t, e.getValue(), 12);
                    if (m != null) {
                        moreBeacons.add(m.getBeacons());
                        processed.add(e.getKey());
                        scanners.add(m.getScannerLocation());
                    }
                }
            }
            processed.forEach(input::remove);
            beacons.add(moreBeacons);
        }
        var allBeacons = beacons.stream()
                .flatMap(List::stream)
                .flatMap(Set::stream).collect(Collectors.toSet());
        LOG.info("solution1: {}", allBeacons.size());

        int max = 0;
        for (int i = 0; i < scanners.size() - 1; i++) {
            for (int j = i + 1; j < scanners.size(); j++) {
                max = Math.max(max, scanners.get(i).add(scanners.get(j).negate()).manhattan());
            }
        }
        LOG.info("solution2: {}", max);
    }
}
