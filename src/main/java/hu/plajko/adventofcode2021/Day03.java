package hu.plajko.adventofcode2021;

import hu.plajko.adventofcode2021.utils.Extensions;
import lombok.SneakyThrows;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day03 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day03.class, args);
    }

    @Override
    @SneakyThrows
    public void run(String... args) {
        var numbers = "day03.in".readLines();

        var g = IntStream.range(0, numbers.get(0).length())
            .mapToObj(i -> count(numbers, i))
            .map(m -> m.get('0') > m.get('1') ? "0" : "1")
            .collect(Collectors.joining());
        var e = g.replaceAll("\\d", m -> m.group().asInt() ^ 1);
        LOG.info("solution1: {}", Long.parseLong(g, 2) * Long.parseLong(e, 2));

        LOG.info("solution2: {}", filter(numbers, true) * filter(numbers, false));
    }

    static Map<Character, Long> count(List<String> numbers, int index) {
        var result = numbers.stream()
            .collect(Collectors.groupingBy(n -> n.charAt(index), Collectors.counting()));
        result.putIfAbsent('0', 0L);
        result.putIfAbsent('1', 0L);
        return result;
    }

    static long filter(List<String> numbers, boolean keepMost) {
        var filtered = numbers;
        String res = null;
        for (int i = 0; i < numbers.get(0).length(); i++) {
            final int _i = i;
            var count = count(filtered, i);
            char keep = (keepMost ^ (count.get('0') > count.get('1'))) ? '0' : '1';
            filtered = filtered.stream().filter(n -> n.charAt(_i) == keep).toList();
            if (filtered.isEmpty()) {
                break;
            } else {
                res = filtered.get(0);
            }
        }
        return Long.parseLong(res, 2);
    }
}
