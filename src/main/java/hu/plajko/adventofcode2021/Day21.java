package hu.plajko.adventofcode2021;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import hu.plajko.adventofcode2021.utils.Extensions;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day21 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day21.class, args);
    }

    @Override
    @SneakyThrows
    public void run(String... args) {
        var playerPositions = "day21.in".readLines().stream()
            .map("Player (\\d+) starting position: (\\d+)".asMatcher())
                .collect(Collectors.toMap(
                    m -> m.g(1).asInt(),
                    m -> m.g(2).asInt()));

        int[] positions = new int[]{
                playerPositions.get(1), // goes first
                playerPositions.get(2)};
        int[] scores = new int[2];
        int rollCount = 0;
        while (scores[1] < 1000) {
            int i = rollCount++ % 100 + 1;
            int j = rollCount++ % 100 + 1;
            int k = rollCount++ % 100 + 1;
            int newPosition = (positions[0] + i + j + k - 1) % 10 + 1;
            positions = new int[]{positions[1], newPosition};
            scores = new int[]{scores[1], scores[0] + newPosition};
        }
        LOG.info("solution1: {}", Math.min(scores[0], scores[1]) * rollCount);

        int targetScore = 21;
        var wins = countWins(List.of(
                new State(playerPositions.get(1), targetScore), // goes first
                new State(playerPositions.get(2), targetScore)),
                CacheBuilder.newBuilder().build());
        LOG.info("solution2: {}", Math.max(wins[0], wins[1]));
    }

    @Value
    static class State {
        int position;
        int scoreNeedToWin;
    }

    @SneakyThrows
    long[] countWins(List<State> states, Cache<List<State>, long[]> cache) {
        var wins = new long[2];
        for (int i = 1; i <= 3; i++) {
            for (int j = 1; j <= 3; j++) {
                for (int k = 1; k <= 3; k++) {
                    int newPosition = (states.get(0).getPosition() + i + j + k - 1) % 10 + 1;
                    int scoreNeedToWin = states.get(0).getScoreNeedToWin() - newPosition;
                    if (scoreNeedToWin <= 0) {
                        wins[0]++;
                    } else {
                        var nextStates = List.of(states.get(1), new State(newPosition, scoreNeedToWin));
                        var result = cache.get(nextStates, () -> countWins(nextStates, cache));
                        wins[0] += result[1];
                        wins[1] += result[0];
                    }
                }
            }
        }
        return wins;
    }
}
