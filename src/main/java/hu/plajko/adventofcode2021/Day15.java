package hu.plajko.adventofcode2021;

import com.google.common.base.Stopwatch;
import hu.plajko.adventofcode2021.utils.Extensions;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import org.jgrapht.Graph;
import org.jgrapht.alg.interfaces.ShortestPathAlgorithm;
import org.jgrapht.alg.shortestpath.AStarShortestPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultDirectedWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.HashMap;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day15 implements CommandLineRunner {

    @Value
    static class Node {
        int x;
        int y;
    }

    public static void main(String[] args) {
        SpringApplication.run(Day15.class, args);
    }

    Graph<Node, DefaultWeightedEdge> buildGraph(List<List<Integer>> input, int multiplier) {
        var nodes = new HashMap<Node, Integer>();
        for (int i = 0; i < multiplier; i++) {
            for (int x = 0; x < input.size(); x++) {
                for (int j = 0; j < multiplier; j++) {
                    var inc = i + j;
                    for (int y = 0; y < input.get(0).size(); y++) {
                        int risk = (input.get(x).get(y) + inc - 1) % 9 + 1;
                        int nx = x + i * input.size();
                        int ny = y + j * input.get(0).size();
                        nodes.put(new Node(nx, ny), risk);
                    }
                }
            }
        }
        Graph<Node, DefaultWeightedEdge> graph
                = new DefaultDirectedWeightedGraph<>(DefaultWeightedEdge.class);
        nodes.keySet().forEach(graph::addVertex);
        nodes.forEach((n, r) -> Stream.of(
            new Node(n.getX() - 1, n.getY()),
            new Node(n.getX() + 1, n.getY()),
            new Node(n.getX(), n.getY() - 1),
            new Node(n.getX(), n.getY() + 1))
                .filter(nodes::containsKey).forEach(target -> {
                    graph.addEdge(n, target);
                    graph.setEdgeWeight(n, target, nodes.get(target));
                }));
        return graph;
    }

    @Override
    @SneakyThrows
    public void run(String... args) {
        var input1 = "day15.in".readLines().stream()
            .map(l -> "\\d".allMatches(l, Integer::parseInt).toList())
            .toList();

        var start = new Node(0, 0);

        var graph1 = buildGraph(input1, 1);
        var dijkstra1 = new DijkstraShortestPath<>(graph1);
        var end1 = new Node(input1.size() - 1, input1.get(0).size() - 1);
        LOG.info("solution1: {}", (int) dijkstra1.getPathWeight(start, end1));

        int multiplier = 5;
        var graph2 = buildGraph(input1, multiplier);


        var end2 = new Node(input1.size() * multiplier - 1, input1.get(0).size() * multiplier - 1);

        List<Function<Graph<Node, DefaultWeightedEdge>, ShortestPathAlgorithm<Node, DefaultWeightedEdge>>> algorithms = List.of(
                DijkstraShortestPath::new,
                g -> new AStarShortestPath<>(g,
                        (n1, n2) -> (Math.abs(n2.getX() - n1.getX()) + Math.abs(n2.getY() - n1.getY())))
//                BellmanFordShortestPath::new,
//                g -> new JohnsonShortestPaths<>(g),
//                FloydWarshallShortestPaths::new,
//                BFSShortestPath::new
        );

        algorithms.forEach(a -> {
            var timer = Stopwatch.createStarted();
            var algorithm = a.apply(graph2);
            LOG.info("solution2: {}", (int) algorithm.getPathWeight(start, end2));
            LOG.info("{}", timer);
        });

//        var path = dijkstra2.getPath(start, end2);
//        BiFunction<Integer, Integer, Object> matrix = (x, y) ->
//                (path.getVertexList().contains(new Node(x, y)) ? "#" : "");
//        matrix.dump(input1.size() * multiplier, input1.get(0).size() * multiplier);
    }
}
