package hu.plajko.adventofcode2021;

import com.google.common.base.Stopwatch;
import hu.plajko.adventofcode2021.utils.Extensions;
import lombok.SneakyThrows;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import org.jgrapht.Graph;
import org.jgrapht.alg.shortestpath.IntVertexDijkstraShortestPath;
import org.jgrapht.graph.DefaultDirectedWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day15a implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day15a.class, args);
    }

    Graph<Integer, DefaultWeightedEdge> buildGraph(List<List<Integer>> input, int multiplier) {
        int width = multiplier * input.get(0).size();
        var nodes = new HashMap<Integer, Integer>();
        for (int i = 0; i < multiplier; i++) {
            for (int x = 0; x < input.size(); x++) {
                for (int j = 0; j < multiplier; j++) {
                    var inc = i + j;
                    for (int y = 0; y < input.get(0).size(); y++) {
                        int risk = (input.get(x).get(y) + inc - 1) % 9 + 1;
                        int nx = x + i * input.size();
                        int ny = y + j * input.get(0).size();
                        int node = nx * width + ny;
                        nodes.put(node, risk);
                    }
                }
            }
        }
        Graph<Integer, DefaultWeightedEdge> graph
                = new DefaultDirectedWeightedGraph<>(DefaultWeightedEdge.class);
        nodes.keySet().forEach(graph::addVertex);
        nodes.forEach((n, r) -> {
            int x = n / (width);
            int y = n % (width);
            List<Integer> neighbours = new ArrayList<>();
            if (x > 0) {
                neighbours.add((x - 1) * width + y);
            }
            if (y > 0) {
                neighbours.add((x) * width + y - 1);
            }
            if (x < multiplier * input.size() - 1) {
                neighbours.add((x + 1) * width + y);
            }
            if (y < width - 1) {
                neighbours.add((x) * width + y + 1);
            }
            neighbours.forEach(target -> {
                    graph.addEdge(n, target);
                    graph.setEdgeWeight(n, target, nodes.get(target));
                });
        });
        return graph;
    }

    @Override
    @SneakyThrows
    public void run(String... args) {
        var input = "day15.in".readLines().stream()
                .map(l -> "\\d".allMatches(l, Integer::parseInt).toList())
                .toList();

        var start = 0;

        var graph1 = buildGraph(input, 1);
        var end1 = input.size() * input.get(0).size() - 1;
        var dijkstra1 = new IntVertexDijkstraShortestPath<>(graph1);
        LOG.info("solution1: {}", (int) dijkstra1.getPathWeight(start, end1));

        int multiplier = 5;
        var timer = Stopwatch.createStarted();
        var graph2 = buildGraph(input, multiplier);
        var end2 = multiplier * multiplier * input.size() * input.get(0).size() - 1;
        var dijkstra2 = new IntVertexDijkstraShortestPath<>(graph2);
        LOG.info("solution2: {}", (int) dijkstra2.getPathWeight(start, end2));
        LOG.info("{}", timer);
    }
}
