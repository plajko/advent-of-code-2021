package hu.plajko.adventofcode2021;

import hu.plajko.adventofcode2021.utils.Extensions;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.Comparator;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day04 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day04.class, args);
    }

    @Override
    @SneakyThrows
    public void run(String... args) {
        var input = "day04.in".readLines();
        var numbers = "\\d+".allMatches(input.remove(0), Integer::parseInt).toList();
        var boards = input.stream()
            .map(l -> "\\d+".allMatches(l, Integer::parseInt).toList())
            .toList()
            .split(List::isEmpty);
        var winners = boards.stream()
            .map(b -> winner(numbers, b))
            .sorted(Comparator.comparing(Winner::getIndex))
            .toList();
        var board1 = winners.first();
        LOG.info("solution1: {}", numbers.get(board1.getIndex()) * board1.getSum().get());
        var board2 = winners.last();
        LOG.info("solution2: {}", numbers.get(board2.getIndex()) * board2.getSum().get());
    }

    @Value
    static class Winner {
        int index;
        Supplier<Integer> sum;
    }

    Winner winner(List<Integer> numbers, List<List<Integer>> board) {
        final int index = Stream.concat(board.stream(), board.rotateCW().stream())
            .filter(numbers::containsAll)
            .mapToInt(line -> line.stream().mapToInt(numbers::indexOf).max().getAsInt())
            .min().getAsInt();
        return new Winner(index, () -> board.flatStream()
            .filter(n -> !numbers.subList(0, index + 1).contains(n))
            .mapToInt(x -> x).sum());
    }

}
