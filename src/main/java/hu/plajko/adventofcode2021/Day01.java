package hu.plajko.adventofcode2021;

import hu.plajko.adventofcode2021.utils.Extensions;
import lombok.SneakyThrows;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.Comparator;
import java.util.List;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day01 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day01.class, args);
    }

    @Override
    @SneakyThrows
    public void run(String... args) {
        var numbers = "day01.in".readLines().stream()
            .map(Integer::parseInt).toList();
        LOG.info("solution1: {}", solve(numbers, 1, (n1, n2) -> n1 > n2 ? 0 : 1));
        LOG.info("solution2: {}", solve(numbers, 3, (n1, n2) -> n1 < n2 ? 1 : 0));
    }

    private int solve(List<Integer> numbers, int windowSize, Comparator<Integer> comparator) {
        int sum = 0;
        for (int i = 0; i < numbers.size() - windowSize; i++) {
            int w1 = numbers.subList(i, i + windowSize).stream().mapToInt(Integer::intValue).sum();
            int w2 = numbers.subList(i + 1, i + windowSize + 1).stream().mapToInt(Integer::intValue).sum();
            sum += Math.max(0, comparator.compare(w1, w2));
        }
        return sum;
    }

}
