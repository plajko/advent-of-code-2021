package hu.plajko.adventofcode2021;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import hu.plajko.adventofcode2021.utils.Extensions;
import lombok.*;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day16 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day16.class, args);
    }

    static class StringDataStream {
        private String s;
        @Getter
        private int index = 0;
        StringDataStream(String s) {
            this.s = s;
        }
        String next(int length) {
            var result = s.substring(index, index + length);
            index += length;
            return result;
        }
    }

    @Value
    static class Result {
        int versionSum;
        List<Long> values;
    }

    @Override
    @SneakyThrows
    public void run(String... args) {
        var input = "day16.in".readLines().get(0);
        var binary = hexToBin(input);
        var result = calc(new StringDataStream(binary), null, 1);
        LOG.info("solution1: {}", result.getVersionSum());
        LOG.info("solution2: {}", Iterables.getOnlyElement(result.getValues()));
    }

    private static Map<Character, String> HEX_TO_BIN =
        ImmutableMap.<Character, String>builder()
            .put('0', "0000").put('1', "0001")
            .put('2', "0010").put('3', "0011")
            .put('4', "0100").put('5', "0101")
            .put('6', "0110").put('7', "0111")
            .put('8', "1000").put('9', "1001")
            .put('A', "1010").put('B', "1011")
            .put('C', "1100").put('D', "1101")
            .put('E', "1110").put('F', "1111")
            .build();

    private String hexToBin(String hex) {
        return hex.chars()
            .mapToObj(c -> (char) c)
            .map(Character::toUpperCase)
            .map(HEX_TO_BIN::get)
            .collect(Collectors.joining());
    }

    Result calc(StringDataStream stream, Integer packetLength, Integer numOfPackets) {
        int startIndex = stream.getIndex();
        int versionSum = 0;
        int packetCounter = 0;
        var values = new ArrayList<Long>();
        while (
                (numOfPackets != null && packetCounter < numOfPackets)
             || (packetLength != null && stream.getIndex() - startIndex < packetLength)
        ) {
            packetCounter++;
            var version = stream.next(3).asInt(2);
            versionSum += version;
            var type = stream.next(3).asInt(2);

            if (type == 4) {
                var groups = new ArrayList<String>();
                groups.add(stream.next(5));
                while (groups.get(groups.size() - 1).startsWith("1")) {
                    groups.add(stream.next(5));
                }
                var literal = groups.stream().map(s -> s.substring(1)).collect(Collectors.joining()).asLong(2);
                values.add(literal);
            } else {
                int typeId = stream.next(1).asInt(2);
                int num = stream.next(typeId == 0 ? 15 : 11).asInt(2);
                var r = calc(stream,
                        typeId == 0 ? num : null,
                        typeId == 1 ? num : null);
                switch (type) {
                    case 0:
                        values.add(r.getValues().stream().mapToLong(x -> x).sum());
                        LOG.debug("sum {} = {}", r.getValues(), values.last());
                        break;
                    case 1:
                        values.add(r.getValues().stream().mapToLong(x -> x).reduce(1, (p, v) -> p * v));
                        LOG.debug("prod {} = {}", r.getValues(), values.last());
                        break;
                    case 2:
                        values.add(r.getValues().stream().mapToLong(x -> x).min().getAsLong());
                        LOG.debug("min {} = {}", r.getValues(), values.last());
                        break;
                    case 3:
                        values.add(r.getValues().stream().mapToLong(x -> x).max().getAsLong());
                        LOG.debug("max {} = {}", r.getValues(), values.last());
                        break;
                    case 5:
                        values.add(r.getValues().get(0) > r.getValues().get(1) ? 1L : 0L);
                        LOG.debug("gt {} = {}", r.getValues(), values.last());
                        break;
                    case 6:
                        values.add(r.getValues().get(0) < r.getValues().get(1) ? 1L : 0L);
                        LOG.debug("lt {} = {}", r.getValues(), values.last());
                        break;
                    case 7:
                        values.add(r.getValues().get(0).equals(r.getValues().get(1)) ? 1L : 0L);
                        LOG.debug("eq {} = {}", r.getValues(), values.last());
                        break;
                }
                versionSum += r.getVersionSum();
            }
        }
        return new Result(versionSum, values);
    }
}
