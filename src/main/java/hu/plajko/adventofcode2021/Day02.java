package hu.plajko.adventofcode2021;

import hu.plajko.adventofcode2021.utils.Extensions;
import lombok.SneakyThrows;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day02 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day02.class, args);
    }

    @Override
    @SneakyThrows
    public void run(String... args) {
        var steps = "day02.in".readLines().stream()
            .map("(.*) (\\d+)".asMatcher()).toList();

        int depth = 0;
        int dist = 0;
        int aim = 0;
        for (var step : steps) {
            int amount = step.g(2).asInt();
            switch (step.g(1)) {
                case "up":
                    aim -= amount;
                    break;
                case "down":
                    aim += amount;
                    break;
                case "forward":
                    dist += amount;
                    depth += amount * aim;
                    break;
            }
        }
        LOG.info("solution1: {}", aim * dist);
        LOG.info("solution2: {}", depth * dist);
    }

}
