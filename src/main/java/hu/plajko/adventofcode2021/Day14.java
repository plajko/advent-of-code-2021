package hu.plajko.adventofcode2021;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import hu.plajko.adventofcode2021.utils.Extensions;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day14 implements CommandLineRunner {

    @Value
    static class State{
        String pair;
        int steps;
    }

    public static void main(String[] args) {
        SpringApplication.run(Day14.class, args);
    }

    @Override
    @SneakyThrows
    public void run(String... args) {
        var input = "day14.in".readLines();
        var s = input.remove(0);
        input.remove(0);
        var mapping = input.stream().map("(.{2}) -> (.)".asMatcher())
                .collect(Collectors.toMap(m -> m.g(1), m -> m.g(2)));

        LOG.info("solution1: {}", count(mapping, s, 10));
        LOG.info("solution2: {}", count(mapping, s, 40));
    }

    long count(Map<String, String> mapping, String s, int steps) {
        var cache = CacheBuilder.newBuilder().<State, Map<Character, Long>>build();
        Map<Character, Long> result = null;
        for (int i = 0; i < s.length() - 1; i++) {
            var currentPair = countPair(
                mapping, cache, "" + s.charAt(i) + s.charAt(i + 1), steps);
            if (result == null) {
                result = currentPair;
            } else {
                result = sum(result, currentPair);
                result.compute(s.charAt(i), (k, v) -> v - 1);
            }
        }
        var values = new ArrayList<>(result.values());
        values.sort(Comparator.naturalOrder());
        return values.get(values.size() - 1) - values.get(0);
    }

    @SneakyThrows
    Map<Character, Long> countPairCached(Map<String, String> mapping, Cache<State, Map<Character, Long>> cache, String pair, int steps) {
        return cache.get(new State(pair, steps), () -> countPair(mapping, cache, pair, steps));
    }

    Map<Character, Long> countPair(Map<String, String> mapping, Cache<State, Map<Character, Long>> cache, String pair, int steps) {
        var insert = mapping.get(pair);
        if (steps == 0) {
            return Stream.of(pair.charAt(0), pair.charAt(1)).collect(
                Collectors.groupingBy(c -> c, Collectors.counting()));
        }
        var c1 = countPairCached(mapping, cache,pair.charAt(0) + insert, steps - 1);
        var c2 = countPairCached(mapping, cache,insert + pair.charAt(1), steps - 1);
        var res =  sum(c1, c2);
        res.compute(insert.charAt(0), (k, v) -> v - 1);
        return res;
    }

    Map<Character, Long> sum(Map<Character, Long> m1, Map<Character, Long> m2) {
        var result = new HashMap<Character, Long>();
        m1.forEach((key, v) -> result.compute(key, (k, current) -> current == null ? v : current + v));
        m2.forEach((key, v) -> result.compute(key, (k, current) -> current == null ? v : current + v));
        return result;
    }

}
