package hu.plajko.adventofcode2021;

import hu.plajko.adventofcode2021.utils.Extensions;
import lombok.SneakyThrows;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.Comparator;
import java.util.Optional;
import java.util.stream.IntStream;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day17 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day17.class, args);
    }

    @Override
    @SneakyThrows
    public void run(String... args) {
        var input = "day17.in".readLines();
        var matcher = "target area: x=(-?\\d+)..(-?\\d+), y=(-?\\d+)..(-?\\d+)".asMatcher().apply(input.get(0));
        int x1 = matcher.g(1).asInt();
        int x2 = matcher.g(2).asInt();
        int y1 = matcher.g(3).asInt();
        int y2 = matcher.g(4).asInt();

        var all = IntStream.range(y1, -y1 * 10).boxed().flatMap(vy ->
            IntStream.range(1, x2)
                .mapToObj(vx -> probeMaxY(x1, x2, y1, y2, vx, vy))
                .filter(Optional::isPresent)
                .map(Optional::get))
            .sorted(Comparator.naturalOrder())
            .toList();

        LOG.info("solution1: {}", all.get(all.size() - 1));
        LOG.info("solution2: {}", all.size());
    }

    Optional<Integer> probeMaxY(int x1, int x2, int y1, int y2, int vx, int vy) {
        int x = 0;
        int y = 0;
        int maxY = 0;
        while (x <= x2 && y >= y1) {
            x += vx;
            y += vy;
            if (y > maxY) {
                maxY = y;
            }
            if (vx > 0) {
                vx--;
            }
            vy--;
            if (x >= x1 && x <= x2 && y >= y1 && y <= y2) {
                return Optional.of(maxY);
            }
        }
        return Optional.empty();
    }

}
