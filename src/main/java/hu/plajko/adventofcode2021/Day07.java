package hu.plajko.adventofcode2021;

import hu.plajko.adventofcode2021.utils.Extensions;
import lombok.SneakyThrows;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day07 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day07.class, args);
    }

    @Override
    @SneakyThrows
    public void run(String... args) {
        var input = "day07.in".readLines();
        var numbers =  "\\d+".allMatches(input.get(0), Integer::parseInt)
                .collect(Collectors.groupingBy(x -> x, TreeMap::new, Collectors.counting()));

        var s1 = IntStream.range(numbers.firstKey(), numbers.lastKey())
            .mapToLong(i -> numbers.entrySet().stream()
                .mapToLong(e ->  Math.abs(e.getKey() - i) * e.getValue())
                .sum())
            .min().orElse(0);
        var s2 = IntStream.range(numbers.firstKey(), numbers.lastKey())
            .mapToLong(i -> numbers.entrySet().stream()
                .mapToLong(e -> {
                        var dist = Math.abs(e.getKey() - i);
                        return dist * (dist + 1) / 2 * e.getValue();
                    })
                .sum())
            .min().orElse(0);
        LOG.info("solution1: {}", s1);
        LOG.info("solution2: {}", s2);
    }
}
