package hu.plajko.adventofcode2021;

import hu.plajko.adventofcode2021.utils.Extensions;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.BitSet;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day22 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day22.class, args);
    }

    @Value
    static class Instruction {
        boolean on;
        int x1;
        int x2;
        int y1;
        int y2;
        int z1;
        int z2;
    }

    @Override
    @SneakyThrows
    public void run(String... args) {
        var input = "day22.in".readLines().stream()
            .map("(.*) x=(.*)\\.\\.(.*),y=(.*)\\.\\.(.*),z=(.*)\\.\\.(.*)".asMatcher())
            .map(m-> new Instruction(
                m.g(1).equals("on"),
                m.g(2).asInt(),m.g(3).asInt(),
                m.g(4).asInt(),m.g(5).asInt(),
                m.g(6).asInt(),m.g(7).asInt()))
            .toList();

        var xc = Stream.concat(Stream.of(-50, 51), input.stream().flatMap(i -> Stream.of(i.getX1(), i.getX2() + 1))).distinct().sorted().toList();
        var yc = Stream.concat(Stream.of(-50, 51), input.stream().flatMap(i -> Stream.of(i.getY1(), i.getY2() + 1))).distinct().sorted().toList();
        var zc = Stream.concat(Stream.of(-50, 51), input.stream().flatMap(i -> Stream.of(i.getZ1(), i.getZ2() + 1))).distinct().sorted().toList();

        var xcLookup = IntStream.range(0, xc.size()).boxed().collect(Collectors.toMap(xc::get, i -> i));
        var ycLookup = IntStream.range(0, yc.size()).boxed().collect(Collectors.toMap(yc::get, i -> i));
        var zcLookup = IntStream.range(0, zc.size()).boxed().collect(Collectors.toMap(zc::get, i -> i));
        var reducedInput = input.stream().map(i -> new Instruction(
            i.isOn(),
            xcLookup.get(i.getX1()), xcLookup.get(i.getX2() + 1),
            ycLookup.get(i.getY1()), ycLookup.get(i.getY2() + 1),
            zcLookup.get(i.getZ1()), zcLookup.get(i.getZ2() + 1)))
        .toList();

        var on = new BitSet();
        reducedInput.forEach(in -> {
            LOG.debug("{}", in);
            IntStream.range(in.getX1(), in.getX2()).forEach(x ->
                IntStream.range(in.getY1(), in.getY2()).forEach(y -> {
                    int from = (x * yc.size() + y) * zc.size() + in.getZ1();
                    int to = (x * yc.size() + y) * zc.size() + in.getZ2();
                    if (in.isOn()) {
                        on.set(from, to);
                    } else {
                        on.clear(from, to);
                    }
                }));
        });
        LOG.debug("counting {} items...", on.length());

        Function<Predicate<int[]>, Long> counter = condition -> on.stream().parallel().mapToObj(n -> {
            int xy = n / zc.size();
            int x = xy / yc.size();
            int y = xy % yc.size();
            int z = n % zc.size();
            return new int[]{x, y, z};
        }).filter(condition).mapToLong(p -> {
            int xd = xc.get(p[0] + 1) - xc.get(p[0]);
            int yd = yc.get(p[1] + 1) - yc.get(p[1]);
            int zd = zc.get(p[2] + 1) - zc.get(p[2]);
            return (long) xd * yd * zd;
        }).sum();

        LOG.info("solution1: {}", counter.apply(p ->
                p[0] >= xcLookup.get(-50) && p[0] < xcLookup.get(51) &&
                p[1] >= ycLookup.get(-50) && p[1] < ycLookup.get(51) &&
                p[2] >= zcLookup.get(-50) && p[2] < zcLookup.get(51)));
        LOG.info("solution2: {}", counter.apply(c -> true));
    }
}
