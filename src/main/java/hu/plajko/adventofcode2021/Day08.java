package hu.plajko.adventofcode2021;

import com.google.common.collect.Sets;
import hu.plajko.adventofcode2021.utils.Extensions;
import lombok.SneakyThrows;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day08 implements CommandLineRunner {

    private static final Function<String, Set<Character>> toCharSet = s -> s.chars().mapToObj(e -> (char) e).collect(Collectors.toSet());

    public static void main(String[] args) {
        SpringApplication.run(Day08.class, args);
    }

    @Override
    @SneakyThrows
    public void run(String... args) {
        var input = "day08.in".readLines();
        var numbers =  input.stream().map(s -> s.split("\\|")).map(sp -> List.of(
            Arrays.asList(sp[0].trim().split(" +")),
            Arrays.asList(sp[1].trim().split(" +"))))
            .toList();

        var s1 = numbers.stream().mapToLong(l -> l.get(1).stream().map(String::length).filter(Set.of(2, 4, 3, 7)::contains).count()).sum();
        LOG.info("solution1: {}", s1);

        var s2 = numbers.stream().mapToLong(l -> {
            var map = find(l.get(0));
            var s = l.get(1).stream().map(toCharSet).map(num -> {
                var entry = map.entrySet().stream().filter(e -> e.getValue().equals(num)).findFirst().get();
                return entry.getKey() + "";
            }).collect(Collectors.joining());
            return Long.parseLong(s);
        }).sum();
        LOG.info("solution2: {}", s2);

    }

    Map<Integer, Set<Character>> find(List<String> digits) {
        var byLength = digits.stream().collect(Collectors.groupingBy(
                String::length,
                Collectors.mapping(toCharSet, Collectors.toList())));
        Map<Integer, Set<Character>> result = new HashMap<>();
        var one = byLength.get(2).get(0);
        var four = byLength.get(4).get(0);
        var seven = byLength.get(3).get(0);
        var eight = byLength.get(7).get(0);
        result.put(1, one);
        result.put(4, four);
        result.put(7, seven);
        result.put(8, eight);
        var three = byLength.get(5).stream().filter(d -> d.containsAll(one)).findFirst().get();
        result.put(3, three);
        byLength.get(5).remove(three);

        var fourDiscriminator = four.stream().filter(c -> !three.contains(c)).findFirst().get();
        if (byLength.get(5).get(0).contains(fourDiscriminator)) {
            result.put(5, byLength.get(5).get(0));
            result.put(2, byLength.get(5).get(1));
        } else {
            result.put(2, byLength.get(5).get(0));
            result.put(5, byLength.get(5).get(1));
        }
        var six = byLength.get(6).stream()
            .filter(s -> Sets.union(s, one).size() == 7)
            .findFirst().get();
        result.put(6, six);
        byLength.get(6).remove(six);

        var zero = byLength.get(6).stream()
            .filter(s -> Sets.union(s, four).size() == 7)
            .findFirst().get();
        result.put(0, zero);
        byLength.get(6).remove(zero);
        result.put(9, byLength.get(6).get(0));
        assert result.size() == 10;
        return result;
    }


}
