package hu.plajko.adventofcode2021;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import hu.plajko.adventofcode2021.utils.Extensions;
import lombok.SneakyThrows;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.*;
import java.util.regex.Pattern;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day12 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day12.class, args);
    }

    @Override
    @SneakyThrows
    public void run(String... args) {
        var caves = MultimapBuilder.hashKeys().hashSetValues().<String, String>build();
        "day12.in".readLines().stream().map("(.*)-(.*)".asMatcher()).forEach(m -> {
             caves.put(m.group(1), m.group(2));
             caves.put(m.group(2), m.group(1));
        });
        LOG.info("solution1: {}", countRoutes(caves, "start", new HashMap<>(), true));
        Runnable r = () -> {
            var timer = Stopwatch.createStarted();
            LOG.info("solution2: {}", countRoutes(caves, "start", new HashMap<>(), false));
            LOG.info("{}", timer);
        };
        r.run();
        r.run();
        r.run();
        r.run();
    }

    static final Pattern lowerCase = Pattern.compile("[a-z]*");

    private long countRoutes(Multimap<String, String> caves, String current, Map<String, Integer> visited, boolean singleSmallTwice) {
        if (current.equals("end")) {
            return 1;
        }
        if ("start".equals(current) && !visited.isEmpty()) {
            return 0;
        }
        if (!current.equals("start") && lowerCase.matcher(current).matches()) {
            int count = Optional.ofNullable(visited.get(current)).orElse(0);
            if (count > 1) {
                return 0;
            }
            if (count == 1) {
                if (singleSmallTwice) {
                    return 0;
                }
                singleSmallTwice = true;
            }
        }

        visited.compute(current, (k, v) -> v == null ? 1 : v + 1);

        final boolean _singleSmallTwice = singleSmallTwice;
        return caves.get(current).stream().parallel().mapToLong(next ->
            countRoutes(caves, next, new HashMap<>(visited), _singleSmallTwice)
        ).sum();
    }
}
