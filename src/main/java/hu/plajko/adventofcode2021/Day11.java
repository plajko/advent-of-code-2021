package hu.plajko.adventofcode2021;

import hu.plajko.adventofcode2021.utils.Extensions;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day11 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day11.class, args);
    }

    @Data
    @AllArgsConstructor
    static class Octopus {
        int energy;
        int flashStep;
        int flash;

        final List<Octopus> neighbours = new ArrayList<>();

        boolean tryFlash(int step) {
            if (this.energy == 10) {
                this.energy = 0;
                this.flashStep = step;
                this.flash++;
                neighbours.forEach(o -> o.incEnergy(step));
                return true;
            }
            return false;
        }

        void incEnergy(int step) {
            if (this.energy == 0 && step == this.flashStep) {
                return;
            }
            if (!tryFlash(step)) {
                energy++;
                tryFlash(step);
            }
        }
    }

    private List<List<Octopus>> readInput() {
        var input = "day11.in".readLines().stream()
                .map(l -> "\\d".allMatches(l).map(m -> new Octopus(m.group().asInt(), 0, 0)).toList())
                .toList();
        for (int x = 0; x < input.size(); x++) {
            for (int y = 0; y < input.get(0).size(); y++) {
                var o = input.get(x).get(y);
                for (int i = -1; i < 2; i++) {
                    for (int j = -1; j < 2; j++) {
                        if (x + i >= 0 && x + i <= input.size() - 1
                         && y + j >= 0 && y + j <= input.get(0).size() - 1
                         && (i != 0 || j != 0)) {
                            o.getNeighbours().add(input.get(x + i).get(y + j));
                        }
                    }
                }
            }
        }
        return input;
    }

    @Override
    @SneakyThrows
    public void run(String... args) {
        val input1 = readInput();
        IntStream.rangeClosed(1, 100).forEach(i -> input1.forEach(l -> l.forEach(o -> o.incEnergy(i))));
        LOG.info("solution1: {}", input1.stream().mapToLong(l -> l.stream().mapToLong(Octopus::getFlash).sum()).sum());

        val input2 = readInput();
        var solution2 = IntStream.iterate(1, i -> i + 1).filter(i -> {
                input2.forEach(l -> l.forEach(o -> o.incEnergy(i)));
                return input2.stream().allMatch(l -> l.stream().allMatch(o -> o.getEnergy() == 0));
            }).findFirst().orElse(0);
        LOG.info("solution2: {}", solution2);
    }
}
