package hu.plajko.adventofcode2021;

import hu.plajko.adventofcode2021.utils.Extensions;
import lombok.SneakyThrows;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day06 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day06.class, args);
    }

    @Override
    @SneakyThrows
    public void run(String... args) {
        var input = "day06.in".readLines();
        var fishes =  "\\d+".allMatches(input.remove(0), Integer::parseInt)
                .collect(Collectors.groupingBy(x -> x, Collectors.counting()));

        LOG.info("solution1: {}", solve(fishes, 80));
        LOG.info("solution2: {}", solve(fishes, 256));
    }

    long solve(Map<Integer, Long> fishes, int days) {
        var current = new HashMap<>(fishes);
        for (int i = 0; i < days; i++) {
            var next = new HashMap<Integer, Long>();
            for (int timer = 0; timer < 9; timer++) {
                if (!current.containsKey(timer)) {
                    continue;
                }
                var count = current.get(timer);
                if (timer == 0) {
                    next.compute(6, (k, v) -> v == null ? count : v + count);
                    next.put(8, count);
                } else {
                    next.compute(timer - 1, (k, v) -> v == null ? count : v + count);
                }
            }
            current = next;
        }
        return current.values().stream().mapToLong(x -> x).sum();
    }
}
