package hu.plajko.adventofcode2021;

import hu.plajko.adventofcode2021.utils.Extensions;
import lombok.SneakyThrows;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.Collections;
import java.util.Map;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day10 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day10.class, args);
    }

    Map<Integer, Long> incompleteScores = Map.of(
        (int)'(', 1L,
        (int)'[', 2L,
        (int)'{', 3L,
        (int)'<', 4L);

    @Override
    @SneakyThrows
    public void run(String... args) {
        var input = "day10.in".readLines();
        LOG.info("solution1: {}", input.stream()
            .map(this::reduce)
            .mapToLong(this::countIllegal)
            .sum());
        var scores = input.stream()
            .map(this::reduce)
            .filter(reduced -> countIllegal(reduced) == 0)
            .map(incomplete -> new StringBuilder(incomplete).reverse().chars()
                .mapToLong(incompleteScores::get)
                .reduce(0L, (score, incompleteScore) -> score * 5 + incompleteScore))
            .toList();
        Collections.sort(scores);
        LOG.info("solution2: {}",  scores.get(scores.size() / 2));
    }

    String reduce(String s) {
        var pattern = Pattern.compile("\\(\\)|\\[]|<>|\\{}");
        var current = s;
        while (true) {
            var next = pattern.matcher(current).replaceAll("");
            if (next.equals(current)) {
                return current;
            } else {
                current = next;
            }
        }
    }

    Map<String, Long> illegalScores = Map.of(
        ")", 3L,
        "]", 57L,
        "}", 1197L,
        ">", 25137L);

    long countIllegal(String reduced) {
        var illegal = "\\]|\\}|\\)|>".allMatches(reduced).map(MatchResult::group).toList();
        if (illegal.isEmpty()) {
            return 0;
        }
        return illegalScores.get(illegal.get(0));
    }
}
