package hu.plajko.adventofcode2021;

import hu.plajko.adventofcode2021.utils.Extensions;
import lombok.SneakyThrows;
import lombok.experimental.ExtensionMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

@Slf4j
@ExtensionMethod({Extensions.class})
public class Day18 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day18.class, args);
    }

    @Override
    @SneakyThrows
    public void run(String... args) {
        var input = "day18.in".readLines();

        String result = input.get(0);
        for (int i = 1; i < input.size(); i++) {
            result = explode(add(result, input.get(i)));
        }
        LOG.info("solution1: {}", sum(result));

        int max = 0;
        for (int i = 0; i < input.size(); i++) {
            for (int j = 0; j < input.size(); j++) {
                if (i == j) {
                    continue;
                }
                max = Math.max(max, sum(explode(add(input.get(i), input.get(j)))));
            }
        }
        LOG.info("solution2: {}", max);
    }

    String explode(String s) {
        return "\\[(\\d+),(\\d+)\\]".allMatches(s).filter(m -> {
            long open = s.substring(0, m.start()).chars().filter(c -> c == '[').count();
            long close = s.substring(0, m.start()).chars().filter(c -> c == ']').count();
            return open - close > 3;
        }).findFirst().map(toExplode -> {
            var prefix = s.substring(0, toExplode.start()).replaceAll("(\\d+)(?=[^\\d]*$)",
                m -> m.g(1).asInt() + toExplode.g(1).asInt());
            var postfix = s.substring(toExplode.end()).replaceAll("(?<=^[^\\d]*)(\\d+)",
                m -> m.g(1).asInt() + toExplode.g(2).asInt());
            return explode(prefix + 0 + postfix);
        }).orElseGet(() -> split(s));
    }

    String split(String s) {
        return "(\\d{2,})".allMatches(s).findFirst().map(toSplit -> {
            int num = toSplit.g(1).asInt();
            int n1 = num / 2;
            int n2 = num - n1;
            return explode(s.substring(0, toSplit.start()) + add(n1, n2) + s.substring(toSplit.end()));
        }).orElse(s);
    }

    String add(Object s1, Object s2) {
        return String.format("[%s,%s]", s1, s2);
    }

    int sum(String s) {
        var result = s.replaceAll("\\[(\\d+),(\\d+)\\]",
            m -> 3 * m.g(1).asInt() + 2 * m.g(2).asInt());
        if (result.matches("\\d+")) {
            return Integer.parseInt(result);
        }
        return sum(result);
    }
}
